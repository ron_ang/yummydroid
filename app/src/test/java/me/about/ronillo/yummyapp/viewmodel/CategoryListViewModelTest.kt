package me.about.ronillo.yummyapp.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import kotlinx.coroutines.ExperimentalCoroutinesApi
import me.about.ronillo.recipeapp.sharedRecipeFramework.entity.CategoryList
import me.about.ronillo.recipeapp.sharedRecipeFramework.usecase.GetCategoryListCommand
import me.about.ronillo.yummyapp.TestCoroutineRule
import me.about.ronillo.yummyapp.ui.home.viewmodel.CategoryListViewModel
import me.about.ronillo.yummyapp.ui.home.viewmodel.Resource
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import java.lang.RuntimeException

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class CategoryListViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var command: GetCategoryListCommand

    @Mock
    private lateinit var observer: Observer<Resource<out CategoryList>>

    @Test
    fun getRecipeList_givenServerResponse200_whenFetch_shouldReturnSuccess() {
        testCoroutineRule.runBlockingTest {
            val categoryList = CategoryList()
            doReturn(categoryList).`when`(command).getCategories()
            val viewModel = CategoryListViewModel(command)
            viewModel.listDataModel.observeForever(observer)
            viewModel.getCategoryList()
            verify(observer).onChanged(Resource(categoryList))
            viewModel.listDataModel.removeObserver(observer)
        }
    }

    @Test
    fun getRecipeList_givenServerResponseError_whenFetch_shouldReturnError() {
        testCoroutineRule.runBlockingTest {
            val errorMessage = "Forced crash"
            val error = RuntimeException(errorMessage)
            doThrow(error).`when`(command).getCategories()
            val viewModel = CategoryListViewModel(command)
            viewModel.listDataModel.observeForever(observer)
            viewModel.getCategoryList()
            verify(observer).onChanged(Resource(null, error))
            viewModel.listDataModel.removeObserver(observer)
        }
    }
}
