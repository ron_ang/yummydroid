package me.about.ronillo.yummyapp.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import kotlinx.coroutines.ExperimentalCoroutinesApi
import me.about.ronillo.recipeapp.sharedRecipeFramework.entity.Recipes
import me.about.ronillo.recipeapp.sharedRecipeFramework.usecase.GetRecipeListCommand
import me.about.ronillo.yummyapp.TestCoroutineRule
import me.about.ronillo.yummyapp.ui.home.viewmodel.RecipeListViewModel
import me.about.ronillo.yummyapp.ui.home.viewmodel.Resource
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import java.lang.RuntimeException

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class RecipeListViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var command: GetRecipeListCommand

    @Mock
    private lateinit var observer: Observer<Resource<out Recipes>>

    @Test
    fun getRecipeList_givenServerResponse200_whenFetch_shouldReturnSuccess() {
        testCoroutineRule.runBlockingTest {
            val recipe = Recipes()
            doReturn(recipe).`when`(command).getRecipes(1)
            val viewModel = RecipeListViewModel(command)
            viewModel.listDataModel.observeForever(observer)
            viewModel.getRecipeList(1)
            verify(observer).onChanged(Resource(recipe))
            viewModel.listDataModel.removeObserver(observer)
        }
    }

    @Test
    fun getRecipeList_givenServerResponseError_whenFetch_shouldReturnError() {
        testCoroutineRule.runBlockingTest {
            val errorMessage = "Forced crash"
            val error = RuntimeException(errorMessage)
            doThrow(error).`when`(command).getRecipes(1)
            val viewModel = RecipeListViewModel(command)
            viewModel.listDataModel.observeForever(observer)
            viewModel.getRecipeList(1)
            verify(observer).onChanged(Resource(null, error))
            viewModel.listDataModel.removeObserver(observer)
        }
    }
}
