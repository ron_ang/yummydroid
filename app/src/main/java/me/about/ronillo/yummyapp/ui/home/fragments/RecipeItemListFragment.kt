package me.about.ronillo.yummyapp.ui.home.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.fragment.findNavController
import coil.compose.AsyncImage
import com.google.android.material.composethemeadapter.MdcTheme
import me.about.ronillo.recipeapp.sharedRecipeFramework.entity.Feed
import me.about.ronillo.yummyapp.R
import me.about.ronillo.yummyapp.ui.home.viewmodel.RecipeListViewModel
import me.about.ronillo.yummyapp.ui.home.viewmodel.ViewModelFactory

class RecipeItemListFragment : Fragment() {

    private var pageStart = 1
    private var viewModel: RecipeListViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                MdcTheme {
                    LazyGrids()
                }
            }
        }
    }

    @Composable
    private fun LazyGrids() {
        activity?.let {
            viewModel = viewModel(factory = ViewModelFactory(it))
            viewModel?.apply {
                val resource by listDataModel.observeAsState()
                resource?.apply {
                    if (error != null) {
                        Toast.makeText(requireContext(), error.message, Toast.LENGTH_LONG).show()
                    } else {
                        if (data != null) {
                            LazyVerticalGrid(columns = GridCells.Adaptive(minSize = 128.dp)) {
                                items(data.feed) { feed ->
                                    RecipeCard(fragment = this@RecipeItemListFragment, feed = feed)
                                }
                            }
                        } else {
                            Card(Modifier.padding(32.dp)) {
                                Column {
                                    Text(text = "Loading...")
                                }
                            }
                        }
                    }
                }
                getRecipeList(pageStart)
            }
        }
    }

    @Composable
    private fun RecipeCard(fragment: Fragment, feed: Feed) {
        Card {
            Column(Modifier.padding(16.dp, 8.dp)) {
                val images = feed.content.details.images
                if (images.isNotEmpty()) {
                    AsyncImage(
                        model = images[0].hostedLargeUrl,
                        contentDescription = null
                    )
                }
                Text(
                    text = feed.display.displayName,
                    modifier = Modifier.clickable {
                        val bundle = Bundle()
                        bundle.putSerializable(
                            RecipeItemDetailFragment.ARG_ITEM_ID,
                            feed
                        )
                        fragment.findNavController().navigate(R.id.fragment_item_detail, bundle)
                    }
                )
            }
        }
    }
}
