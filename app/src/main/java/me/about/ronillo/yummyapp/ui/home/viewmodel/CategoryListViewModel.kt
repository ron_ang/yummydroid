package me.about.ronillo.yummyapp.ui.home.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import me.about.ronillo.recipeapp.sharedRecipeFramework.entity.CategoryList
import me.about.ronillo.recipeapp.sharedRecipeFramework.network.Api
import me.about.ronillo.recipeapp.sharedRecipeFramework.usecase.GetCategoryListCommand
import org.jetbrains.annotations.TestOnly

class CategoryListViewModel : ViewModel {

    private val _listDataModel = MutableLiveData<Resource<out CategoryList>>()
    val listDataModel = _listDataModel

    private var api: Api? = null
    private var command: GetCategoryListCommand

    constructor() : super() {
        api = Api("")
        command = GetCategoryListCommand(api!!, 10000)
    }

    @TestOnly
    constructor(command: GetCategoryListCommand) {
        this.command = command
    }

    fun getCategoryList() {
        viewModelScope.launch {
            val deferred = viewModelScope.async {
                try {
                    val categories = command.getCategories()
                    Resource(categories)
                } catch (e: Exception) {
                    Resource(null, e)
                }
            }

            val resource = deferred.await()
            _listDataModel.value = resource
        }
    }
}
