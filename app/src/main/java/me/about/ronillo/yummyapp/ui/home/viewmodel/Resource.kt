package me.about.ronillo.yummyapp.ui.home.viewmodel

data class Resource<T>(val data: T?, val error: Throwable? = null)