package me.about.ronillo.yummyapp.ui.home.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import me.about.ronillo.recipeapp.sharedRecipeFramework.entity.Recipes
import me.about.ronillo.recipeapp.sharedRecipeFramework.network.Api
import me.about.ronillo.recipeapp.sharedRecipeFramework.usecase.GetRecipeListCommand
import org.jetbrains.annotations.TestOnly

class RecipeListViewModel : ViewModel {

    private val _listDataModel = MutableLiveData<Resource<out Recipes>>()
    val listDataModel = _listDataModel

    private var api: Api? = null
    private var command: GetRecipeListCommand

    constructor() : super() {
        // TODO: get an api key from [Yummly2](https://rapidapi.com/apidojo/api/yummly2)
        api = Api("")
        command = GetRecipeListCommand(api!!, 10000)
    }

    @TestOnly
    constructor(command: GetRecipeListCommand) {
        this.command = command
    }

    fun getRecipeList(pageStart: Int) {
        viewModelScope.launch {
            val deferred = viewModelScope.async {
                try {
                    val recipes = command.getRecipes(pageStart)
                    Resource(recipes)
                } catch (e: Exception) {
                    Resource(null, e)
                }
            }

            val resource = deferred.await()
            _listDataModel.value = resource
        }
    }
}
