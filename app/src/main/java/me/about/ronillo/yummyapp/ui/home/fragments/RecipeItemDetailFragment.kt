package me.about.ronillo.yummyapp.ui.home.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import me.about.ronillo.recipeapp.sharedRecipeFramework.entity.Feed
import me.about.ronillo.yummyapp.databinding.FragmentItemDetailBinding

class RecipeItemDetailFragment : Fragment() {

    private var item: Feed? = null
    private lateinit var itemDetailTextView: TextView
    private var _binding: FragmentItemDetailBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            if (it.containsKey(ARG_ITEM_ID)) {
                item = it.getSerializable(ARG_ITEM_ID) as? Feed
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentItemDetailBinding.inflate(inflater, container, false)
        val rootView = binding.root

        binding.toolbarLayout?.title = item?.content?.details?.name ?: ""

        itemDetailTextView = binding.itemDetail
        item?.let {
            itemDetailTextView.text = it.content.description?.text ?: ""
        }

        return rootView
    }

    companion object {
        /**
         * The fragment argument representing the item ID that this fragment
         * represents.
         */
        const val ARG_ITEM_ID = "feed"
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}