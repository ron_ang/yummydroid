package me.about.ronillo.yummyapp.ui.home.viewmodel

import android.app.Activity
import androidx.lifecycle.ViewModelProvider

class ViewModelFactory(private val context: Activity) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : androidx.lifecycle.ViewModel> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(RecipeListViewModel::class.java) -> {
                RecipeListViewModel() as T
            }
            modelClass.isAssignableFrom(CategoryListViewModel::class.java) -> {
                CategoryListViewModel() as T
            }
            else -> throw IllegalArgumentException("Unknown class name")
        }
    }
}
