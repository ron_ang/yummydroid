# README #

Hi! I'm Ronillo, I am an skilled Android developer and has more than 13 years of experience in the software industry. Visit https://about.me/ronillo to know more about me.

### What is this repository for? ###

* This project demonstrate how to unit tests an ViewModel with LiveData & Kotlin Coroutines that follows the basic MVVM or MVI architecture.
* This project demonstrate how to add Jetpack Compose to tradional Android views.
* This project is a presentation layer and consumes this multi-platform recipe library https://github.com/lucifer-thebeast/yummyandroidlib/releases
* 1.1

### How do I get set up? ###

* Setup the Android SDK
* Download a text editor (such as Atom or Sublime) to open this project.
* You need to get an api key from [Yummly2](https://rapidapi.com/apidojo/api/yummly2)
* Use gradle commands to build and run tests.

### Contribution guidelines ###

* Writing tests
* Code review

### Who do I talk to? ###

* Repo owner or admin
* Message me on [LinkedIn](https://www.linkedin.com/in/ronillo-ang/)